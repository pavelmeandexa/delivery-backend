<?php

namespace App\JsonApi\V1\Drivers;

use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected $resourceType = 'drivers';

    /**
     * @param \App\Driver $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource)
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\Driver $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource)
    {
        return [
            'name' => $resource->name,
            'address' => $resource->address,
            'mobile' => $resource->mobile,
            'area' => $resource->area,
            'created_at' => optional($resource->created_at)->format("Y-m-d H:i:s"),
            'updated_at' => optional($resource->updated_at)->format("Y-m-d H:i:s"),
        ];
    }
}
